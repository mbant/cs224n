# Assigment 2

[Questions](a2.pdf)

## Part 1 - Written

[See the rendered a2_solutions.md](a2_solutions.pdf)

## Part 2 - Code

Use 

```bash
python3 word2vec.py
python3 sgd.py
```

to test the `word2vec` implementation, then run 

```bash
sh get datasets.sh
python3 run.py
```

to download the Stanford Sentiment Treebank (SST) dataset, train the embeddings and later apply them to a simple sentiment analysis task.

The resulting projection of the word vectors into 2 dimensions is  presented below.

![img_w2v](word_vectors.png)
