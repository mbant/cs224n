## Solutions to Part 1

### a)
Because $y_w$ is a one-hot encoded vector we can rewrite

$-\sum \limits_{w \in Vocab} y_w \log \left( \hat y_w \right)$ as $-\sum \limits_{w \in Vocab} \mathbb{1}_o(w) \log \left( \hat y_w \right)$ where $\mathbb{1}_y(x)$ is an indicator function equal to zero everywhere except when $x=y$, where it si equal to one.

We can thus show that

$-\sum \limits_{w \in Vocab} \mathbb{1}_o(w) \log \left( \hat y_w \right) = -\mathbb{1}_o(o) \log \left( \hat y_o \right) -\sum \limits_{w \in Vocab, w \neq o} \mathbb{1}_o(w) \log \left( \hat y_w \right) = -\log \left( \hat y_o \right)$  

### b)

First, let's derive the derivative of the softmax $S(x_i) = \frac{ e^{x_i}}{\sum_{j=1}^N e^{x_j} }$ function in general (for fun):

when $i=k$ $\frac{ dS(x_i)}{dx_k} = \frac{ e^{x_i} \sum_{j=1}^N e^{x_j} - e^{x_i} e^{x_i}}{ \left( \sum_{j=1}^N e^{x_j} \right)^2 } = \frac{ e^{x_i} \left( \sum_{j=1}^N e^{x_j} - e^{x_i} \right)}{ \left( \sum_{j=1}^N e^{x_j} \right)^2 }=\frac{ e^{x_i}}{\sum_{j=1}^N e^{x_j} } \times \frac{\sum_{j=1}^N e^{x_j} - e^{x_i}}{\sum_{j=1}^N e^{x_j} } = \frac{ e^{x_i}}{\sum_{j=1}^N e^{x_j} } \times \left( 1 - \frac{ e^{x_i}}{\sum_{j=1}^N e^{x_j} }\right) = S(x_i) \left(1-S(x_i)\right)$

and when $i\neq k$ $\frac{ dS(x_i)}{dx_k} = \frac{ 0 - e^{x_i} e^{x_k}}{ \left( \sum_{j=1}^N e^{x_j} \right)^2 } = -\frac{e^{x_i}}{\sum_{j=1}^N e^{x_j} } \times \left( \frac{ e^{x_k}}{\sum_{j=1}^N e^{x_j} }\right) = -S(x_i) S(x_k)$

Secondly, calling $z_i = S(x_i)$, we can derive the gradient of the cross-entropy loss by taking

$J = -\sum \limits_{i=1}^N t_i \log(z_i)$ where $t_i$ is the one-hot vector for the target.

$\frac{ \partial J}{\partial x_k} = -\sum \limits_{i=1}^N\frac{ \partial t_i \log(z_i)}{\partial x_k} = -\sum \limits_{i=1}^N \frac{t_i}{z_i}\frac{\partial z_i}{\partial x_k} = - \frac{t_k}{z_k}\frac{\partial z_k}{\partial x_k} -\sum \limits_{i=1, i\neq k}^N \frac{t_i}{z_i}\frac{\partial z_i}{\partial x_k} = - \frac{t_k}{z_k}z_k(1-z_k) - \sum \limits_{i=1, i\neq k}^N \frac{t_i}{z_i} (-z_i z_k) = t_k z_k - t_k + \sum \limits_{i=1, i\neq k}^N t_iz_k = -t_k +  z_k\sum t_i = z_k - t_i$

Notice though that these expression differ from the subsequent answer because here we're assuming $x_i \perp x_j$, while in the word2vec model this is not true in general!

----

Let's shorten $Vocab \rightarrow V$ and call ${\sum_{w\in V} e^{u_w^t v_c} } = \Sigma_V$ for convenience. We have

$$J_{naive-softmax}(o) = -\sum \limits_{w \in V} y_w \log \left( \hat y_w \right) = -\log(\hat y_o)$$
where $\hat y_w = \frac{ e^{u_w^t v_c}}{\Sigma_V}$.

We could now can proceed similarly to before by writing

$\frac{\partial J_{naive-softmax}(o)}{\partial v_c} = -\sum \limits_{w \in V} \frac{\partial \left(y_w \log \left( \hat y_w \right)\right)}{\partial v_c} = ...$ 

But this time we can make use of the fact that we know only $y_o = 1$ and all the others are null; so:

$\frac{\partial J_{naive-softmax}(o)}{\partial v_c} = -\frac{\partial \log(\hat y_o)}{\partial v_c} = -\frac{ \partial \log\left(\frac{ e^{u_o^t v_c}}{\Sigma_V}\right) }{\partial v_c} = -\frac{ \partial \left[ u_o^t v_c -\log\left(\Sigma_V\right) \right] }{\partial v_c} = - u_o + \frac{\sum_{w\in V} e^{u_w^t v_c}u_w}{\Sigma_V} = \sum_{w\in V} \hat y_w u_w - u_o = U^t\hat y - u_o$

Again we can make use of the fact that $y$ is a one-hot vector to finally write

$\frac{\partial J_{naive-softmax}(o)}{\partial v_c} = U^t\hat y - U^ty = U^t(\hat y - y)$

### c)

Similarly to point (b) we write

$\frac{\partial J_{naive-softmax}(o)}{\partial u_w} = -\frac{\partial \log(\hat y_o)}{\partial u_w} = -\frac{ \partial \log\left(\frac{ e^{u_o^t v_c}}{\Sigma_V}\right) }{\partial u_w} = -\frac{ \partial \left[ u_o^t v_c -\log\left(\Sigma_V\right) \right] }{\partial u_w}$

but now we have to distiguish two cases:

For $w=o$ we have

$\frac{\partial J_{naive-softmax}(o)}{\partial u_w} = -\frac{ \partial \left[ u_w^t v_c -\log\left(\Sigma_V\right) \right] }{\partial u_w} = -v_c + \frac{\frac{ \partial \sum_{k\in V} e^{u_{k} v_c}}{\partial u_w}}{\Sigma_V} = -v_c + \frac{ e^{u_w^t v_c}}{\Sigma_V} v_c = -v_c + \hat y_w v_c = (\hat y_w - 1) v_c = (\hat y_o - y_o) v_c$

while for $w \neq o$ the expression simplifies to

$\frac{\partial J_{naive-softmax}(o)}{\partial u_w} = -\frac{ \partial \left[ u_o^t v_c -\log\left(\Sigma_V\right) \right] }{\partial u_w} = \frac{\frac{ \partial \sum_{k\in V} e^{u_{k} v_c}}{\partial u_w}}{\Sigma_V} = \hat y_w v_c = (\hat y_w - y_w) v_c$

Where the last section of both expressions comes from the one-hot vector $y$.

We can thus summaries both cases using $\frac{\partial J_{naive-softmax}(o)}{\partial u_w} = (\hat y - y) v_c$.

### d)

Using the gradient of the softmax computed in point (b) we can esily see that the sigmoid function (which is a special case for $N=2$) come out from

$P(x_1 = 1) = \frac{e^{x_1}}{e^{x_1}+e^{x_2}} = \frac{\frac{e^{x_1}}{e^{x_2}}}{\frac{e^{x_1}+e^{x_2}}{e^{x_2}}} = \frac{e^{(x_1-x_2)}}{e^{(x_1-x_2)}+1}$ and thus by calling $x = x_1-x_2$ we revert back to the sigmoid function $P(x_1 = 1) = \sigma(x) = \frac{e^{x}}{e^{x}+1}$ and by makeing use of the fact that for two classes $P(x_2) = 1-P(x_2)$ we obtain $P(x_2 = 1) = 1- \sigma(x) = 1-\frac{e^{x}}{e^{x}+1} = \frac{1}{e^{x}+1}$.

So we can use the above result and easily derive that

$\frac{ d \sigma(x)}{dx} = \frac{ d \frac{e^{x}}{e^{x}+1}}{dx} = \frac{e^x \left(e^x+1\right) - e^xe^x}{\left(e^x+1\right)^2} = e^x \frac{1}{\left(e^x+1\right)^2} = \sigma(x)\frac{1}{e^x+1} = \sigma(x)(1-\sigma(x))$.

Since we're going to use it later, note that 

$\frac{ d \log(\sigma(x))}{dx} = \frac{ d \left[x - \log(1+e^x)\right]}{dx} = 1 - \frac{e^x}{1+e^x} = 1-\sigma(x)$

### e)

The negative-sampling loss is an alternative that sample $K$ words $w_k$ (that are not $w_o$, hence *negative* words) and uses those to compute the loss rather than minimizing over the whole vocabulary $V$. The  loss is defined as:

$$J_{neg-sampling}(o) = -\log(\sigma(u_o^t v_c)) - \sum \limits_{k=1}^K\log(\sigma(-u_k^t v_c))$$

We can use the above results to guide us in computing its partial derivatives.

$\frac{\partial J_{neg-sampling}(o)}{\partial v_c} = \frac{\partial \left[ -\log(\sigma(u_o^t v_c)) - \sum \limits_{k=1}^K\log(\sigma(-u_k^t v_c))\right]}{\partial v_c} = u_o^t\left(\sigma(u_o^t v_c) -1\right) - \sum \limits_{k=1}^K u_k^t\left(\sigma(u_k^t v_c) -1\right)$


$\frac{\partial J_{neg-sampling}(o)}{\partial u_o} = -\frac{\partial \log(\sigma(u_o^t v_c))}{\partial v_c} = \left(\sigma(u_o^t v_c) -1\right) v_c$


$\frac{\partial J_{neg-sampling}(o)}{\partial u_k} = -\sum \limits_{k'=1}^K \frac{\partial \log(\sigma(-u_{k'}^t v_c))}{\partial v_c} = \left(1 - \sigma(u_k^t v_c)\right) v_c$


### f)

Considering the 
$$J_{skip-gram}(o,t,m) = \sum \limits_{j \in {-m, \dots, m}, j \neq 0} J(t+j)$$
with $J$ either the cross-entropy or the negative-sampling loss.

$\frac{\partial J_{skip-gram}(o,t,m)}{\partial v_c} = \sum \limits_{j \in {-m, \dots, m}, j \neq 0}\frac{\partial J(t+j)}{\partial v_c}$

$\frac{\partial J_{skip-gram}(o,t,m)}{\partial U} = \sum \limits_{j \in {-m, \dots, m}, j \neq 0}\frac{\partial J(t+j)}{\partial U}$

$\frac{\partial J_{skip-gram}(o,t,m)}{\partial v_w} = 0$ when $c \neq w$

