# Assigment 3

[Questions](a3.pdf)

## Part 1 - Written

This time the written answers are quite math-light, so I can just type them in here.

### a) Adam Optimizer

#### i) Momentum

On a high level, momentum helps by **accelerating SGD** in the dimensions (in the parameter space) that have **gradients constantly pulling toward one direction**, while **dampening gradients** (and thus reducing the variance) **when the direction changes a lot** from one batch to the next. This helps SGD for example to navigate through areas of the loss where one direction is much steeper then another.

#### ii) Adaptive Learning Rates

By dividing each dimension by (a moving average of-) their sum of squares we are essentially **normalizing** the gradient updates, upscaling the ones for parameters that receive small gradients and downscaling the updates for weights with large incoming gradients.

This helps because now it makes sense to apply a common learning rate to all parameters, again because the updates are normalized, while before some dimensions might have been updated too slowly because we needed to keep the learning rate low to avoid explosins in other dimensions.

### b) Dropout

#### i) γ and the Dropout probability

Because units are dropped with probability equal to a **Bernoulli with parameter p** and assuming their expectation was X before, now the output has **expectation equal to pX**. We thus need to **rescale them by γ=1/p** to maintain the same expected output.

#### ii) Dropout during evaluation/inference

**Dropout helps regularization** (amongst possibly other reasons) by allowing the network not to rely on a single activation to learn a particular feature, but rather helps in building some sort of redundancy.

**During inference though we want all possible informations** that can help us in predicting the correct output and thus **we don't want to use Dropout** on any of our units. Similarly, because we're not using Dropout anymore, **we don't need to rescale by γ** anymore.

## Part 2 - Dependency Parser

### a) I parsed this sentence correctly

| Stack                          | Buffer                                 | New dependency      | Transition     |step|
|--------------------------------|----------------------------------------|---------------------|----------------|----|
| [ROOT]                         | [I, parsed, this, sentence, correctly] |                     | Initial config |0   |
| [ROOT, I]                      | [parsed, this, sentence, correctly]    |                     | SHIFT          |1   |
| [ROOT, I, parsed]              | [this, sentence, correctly]            |                     | SHIFT          |2   |
| [ROOT, parsed]                 | [this, sentence, correctly]            | parsed -> I         | LEFT-ARC       |3   |
| [ROOT, parsed, this]           | [ sentence, correctly]                 |                     | SHIFT          |4   |
| [ROOT, parsed, this, sentence] |           [correctly]                  |                     | SHIFT          |5   |
| [ROOT, parsed, sentence]       |           [correctly]                  | sentence -> this    | LEFT-ARC       |6   |
| [ROOT, parsed]                 |           [correctly]                  | parsed -> sentence  | RIGHT-ARC      |7   |
| [ROOT, parsed, correctly]      |           []                           |                     | SHIFT          |8   |
| [ROOT, parsed]                 |           []                           | parsed -> correctly | RIGHT-ARC      |9   |
| [ROOT]                         |           []                           | ROOT -> parsed      | RIGHT-ARC      |10  |

### b) How many steps to parse a sentece with *n* words?

We'll need 2*n* steps to parse such a sentence, because *n* SHIFT operations are needed to move all words from the buffer to the stack and again *n* steps are necessary to perform add an ARC (either LEFT-ARC or RIGHT-ARC) and finally leave again only the ROOT as the only token in the stack.

### c-d-e) Coding a Neural Dependency Parser

See implementation in `parser_transitions.py`, `parser_model.py` and 'run.py`.
Final loss and UAS values after 10 epochs are as reported below:

    Train Loss: 0.05912443948304885
    Dev UAS: 88.29
    Test UAS: 88.95

By using a larger/deeper model we could improve on the UAS score to get closer to [the original paper](http://cs.stanford.edu/people/danqi/papers/emnlp2014.pdf)'s results. I thus trained then another model substituting the single hidden linear layers with 200 units with 3 linear layers (followed by ReLU and Dropout) with respectively 512,1024 and 512 units. The output of the first and third layers (skip connection) are summed and as before the final hidden output gets into a linear layer that produces the logits for each of the 3 classes.
I also used Kaiming init rather than Xavier to properly account for the ReLUs.

I get the following results after 10 epochs:

    Train Loss: 0.06310333224876231
    Dev UAS: 89.71
    Test UAS: 90.09

Loss was still decreasing on the validation set so maybe more epochs would lead to higher UAS.

### f) Parsing errors

#### i)

![vpae](i.png)

**Error Type**: Verb Phrase Attachment Error
**Incorrect Dependency**: wedding -> fearing
**Correct Dependency**: heading -> fearing

#### ii)

![cae](ii.png)

**Error Type**: Coordination Attachment Error
**Incorrect Dependency**: makes -> rescue
**Correct Dependency**: rush -> rescue

#### iii)

![ppae](iii.png)

**Error Type**: Prepositional Phrase Attachment Error
**Incorrect Dependency**: named -> Midland
**Correct Dependency**: guy -> Midland

I also thought *is* might be the root here with *on loan* modifying that, but what do I know...

#### iv)

![mae](iv.png)

**Error Type**: Modifier Attachment Error
**Incorrect Dependency**: elements -> most 
**Correct Dependency**: crucial -> most