#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS224N 2018-19: Homework 5
model_embeddings.py: Embeddings for the NMT model
Pencheng Yin <pcyin@cs.cmu.edu>
Sahil Chopra <schopra8@stanford.edu>
Anand Dhoot <anandd@stanford.edu>
Michael Hahn <mhahn2@stanford.edu>
"""

import torch.nn as nn

# Do not change these imports; your module names should be
#   `CNN` in the file `cnn.py`
#   `Highway` in the file `highway.py`
# Uncomment the following two imports once you're ready to run part 1(j)

from cnn import CNN
from highway import Highway

# End "do not change" 

class ModelEmbeddings(nn.Module): 
    """
    Class that converts input words to their CNN-based embeddings.
    """
    def __init__(self, embed_size, vocab):
        """
        Init the Embedding layer for one language
        @param embed_size (int): Embedding size (dimensionality) for the output 
        @param vocab (VocabEntry): VocabEntry object. See vocab.py for documentation.
        """
        super(ModelEmbeddings, self).__init__()

        ## A4 code
        # pad_token_idx = vocab.src['<pad>']
        # self.embeddings = nn.Embedding(len(vocab.src), embed_size, padding_idx=pad_token_idx)
        ## End A4 code

        ### YOUR CODE HERE for part 1j
        self.embed_size = embed_size
        self.vocab = vocab
        # other useful quantities
        self.pad_token_idx = self.vocab.char2id['<pad>']
        self.char_embed_size = 50
        self.word_length = 21
        self.dropout_p = 0.3

        self.char_embed = nn.Embedding(
            num_embeddings=len(self.vocab.char2id),
            embedding_dim=self.char_embed_size,
            padding_idx=self.pad_token_idx)

        self.conv = CNN(
            char_embed_size=self.char_embed_size,
            out_filters=self.embed_size,
            word_length=self.word_length)

        self.highway = Highway(
            embed_size=self.embed_size)

        self.dropout = nn.Dropout(
            p=self.dropout_p)

        ### END YOUR CODE

    def forward(self, x_padded):
        """
        Looks up character-based CNN embeddings for the words in a batch of sentences.
        @param x_padded: Tensor of integers of shape (sentence_length, batch_size, word_length) where
            each integer is an index into the character vocabulary

        @param x_word_emb: Tensor of shape (sentence_length, batch_size, embed_size), containing the 
            CNN-based embeddings for each word of the sentences in the batch
        """
        ## A4 code
        # output = self.embeddings(x_padded)
        # return output
        ## End A4 code

        ### YOUR CODE HERE for part 1j

        x_emb = self.char_embed(x_padded) # (sent_length,batch_size,word_length,char_embed_size)
        sent_length, batch_size, _, _ = x_emb.shape

        x_reshaped = x_emb.view(
            sent_length*batch_size,self.word_length,self.char_embed_size
            ).transpose(1,2) # (sent_length*batch_size,char_embed_size,word_length)
        
        x_conv_out = self.conv(x_reshaped) # (sent_length*batch_size,embed_size) 
        x_highway = self.highway(x_conv_out) # (batch_size,embed_size) 
        x_word_emb = self.dropout(x_highway)

        return x_word_emb.view(sent_length,batch_size,self.embed_size)
        ### END YOUR CODE