import torch
from torch import nn
from torch.nn import functional as F

from highway import Highway
from cnn import CNN
from model_embeddings import ModelEmbeddings

print ("-"*80)
print("Running Sanity Check for the Custom Highway and CNN Modules")
print ("-"*80)

print("Simple checks for dimensions")
## Silly example just to check dimensions

batch_size = 5
e_word_size = 15
hw_layer = Highway(e_word_size)

sample_batch = torch.randn(batch_size,e_word_size)
out = hw_layer(sample_batch)

assert( out.shape == sample_batch.shape ), f"Output dimensions don't match for Highway layer. Expected {sample_batch.shape} but got {out.shape} instead!"

e_char_size = 10
word_length = 21
cnn_layer = CNN(e_char_size,e_word_size,word_length,5)

sample_batch = torch.randn(batch_size,e_char_size,word_length)
out = cnn_layer(sample_batch)

exp_size = torch.Size([batch_size,e_word_size])
assert( out.shape == exp_size ), f"Output dimensions don't match for CNN layer. Expected {exp_size} but got {out.shape} instead!"

print("Passed")
print ("-"*80)

## More complex example using a real batch of sentences
import json
import math
import pickle
import sys
import time

import numpy as np

from docopt import docopt
from typing import List, Tuple, Dict, Set, Union
from tqdm import tqdm
from utils import pad_sents_char, read_corpus, batch_iter
from vocab import Vocab, VocabEntry

BATCH_SIZE = 1
C_EMBED_SIZE = 3
W_EMBED_SIZE = 4
WORD_LENGTH = 21

class DummyVocab():
    def __init__(self):
        self.char2id = json.load(open('./sanity_check_en_es_data/char_vocab_sanity_check.json', 'r'))
        self.id2char = {id: char for char, id in self.char2id.items()}
        self.char_unk = self.char2id['<unk>']
        self.start_of_word = self.char2id["{"]
        self.end_of_word = self.char2id["}"]


# Seed the Random Number Generators
seed = 1234
torch.manual_seed(seed)
torch.cuda.manual_seed(seed)
np.random.seed(seed * 13 // 7)

# vocab = Vocab.load('./sanity_check_en_es_data/vocab_sanity_check.json') 
char_vocab = DummyVocab()
vocab = VocabEntry()

print("Running test on a list of sentences")
sentences = [['Human:', 'What', 'do', 'we', 'want?'], ['Computer:', 'Natural', 'language', 'processing!'], ['Human:', 'When', 'do', 'we', 'want', 'it?'], ['Computer:', 'When', 'do', 'we', 'want', 'what?']]
max_sent_length = max(map(len,sentences))
# testing to_input_tensor_char 
sample_input_tensor = vocab.to_input_tensor_char(sentences,torch.device('cpu'))

exp_size = torch.Size([max_sent_length,W_EMBED_SIZE,WORD_LENGTH])
assert( exp_size == sample_input_tensor.shape ), f"to_input_tensor_char produces wrong dimensions. Expected {exp_size} but got {sample_input_tensor.shape} instead!"

# me = ModelEmbeddings(W_EMBED_SIZE,char_vocab)
# out = me(sample_input_tensor.unsqueeze(1)) # batch size of 1 

# exp_size = torch.Size([max_sent_length,BATCH_SIZE,W_EMBED_SIZE])
# assert (), f"Output dimensions don't match for Embedding layer. Expected {exp_size} but got {out.shape} instead!"

# print("Passed")
