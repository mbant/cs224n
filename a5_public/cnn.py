#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS224N 2018-19: Homework 5
"""

import torch
from torch import nn
from torch.nn import functional as F

### YOUR CODE HERE for part 1h

class CNN(nn.Module):
    """ 
    """
    def __init__(self, char_embed_size: int, out_filters: int, word_length: int, kernel_size : int = 5):
        """ Init function for the CNN module:

        note that the convolution happens over the first dimension for 
        each sample in the batch, so there's no need to specify it

        @param char_embed_size: number of input channels/filters, e_char in the PDF
        @param out_filters: number of output filters, f or e_word in PDF
        @param kernel_size: for the Conv layer, also called window_size
        @param word_length: number of characters in each word, 
            this is the dimension we're applying the convolution over
            m_word in the PDF
        """
        super(CNN, self).__init__()
        self.char_embed_size = char_embed_size
        self.out_filters = out_filters
        self.kernel_size = kernel_size
        self.word_length = word_length

        self.conv = nn.Conv1d(char_embed_size,out_filters,kernel_size,bias=True)
        self.max_pool = nn.MaxPool1d(word_length - kernel_size + 1) # assuming no padding and stride 1

    def forward(self,x_reshaped):
        """ Forward function for the CNN module

        We apply a temporal convolution (over the word_length dimension) to x_reshaped
        followed by a ReLU and MaxPooling to produce x_conv_out

        @param x_reshaped: input tensor with a batch of word_length embedded characters 
            with expected dimension (batch_size,char_embed_size,word_length)

        @return x_conv_out: output tensor with dimension (batch_size,out_filters) 
        """
        x_conv = self.conv(x_reshaped)
        x_conv_out = self.max_pool( F.relu(x_conv) ).squeeze(-1)

        return x_conv_out

### END YOUR CODE

