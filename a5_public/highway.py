#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS224N 2018-19: Homework 5
"""

import torch
from torch import nn
from torch.nn import functional as F

### YOUR CODE HERE for part 1h

class Highway(nn.Module):
    """ Simple Highway network module for an NMT model
        Skip-connection module controlled by a dinamyc gate
    """
    def __init__(self, embed_size: int):
        """ Init function for the Highway module:

        @param embed_size the dimension of the word embedding (e_word in the PDF)
        """
        super(Highway, self).__init__()

        self.embed_size = embed_size
        self.project = nn.Linear(self.embed_size,self.embed_size,bias=True)
        self.gatefy = nn.Linear(self.embed_size,self.embed_size,bias=True)

    def forward(self,x_conv_out):
        """ Forward function for the Highway module

        The input is passed through two different linear layers that preserve 
        dimensions to form a projection and a gate. We use the gate to combine
        the projection and the original input.

        @param x_conv_out is the output of the convolutional part of the network
            it has dimensions (batch_size, e_word)

        @return x_highway of dims (batch_size, e_word)
        """
        x_proj = F.relu(self.project(x_conv_out))
        x_gate = torch.sigmoid(self.gatefy(x_conv_out))

        x_highway = x_gate * x_proj + (1-x_gate) * x_conv_out

        return x_highway

### END YOUR CODE 

