# Assigment 5

[Questions](a5.pdf)


## Part 1: Character-based convolutional encoder for NMT

### Written questions

#### a)

Word embedding need to encode the meaning of a whole word, possibly taking into account the surrounding context as well.
Characted embeddings help the NN by representing a character as a sequence of numbers, as did the word embeddings, but this time the object to be represented has a much 'lower dimensional meaning' in some sense and the context around it, still in terms of characters, is probably almost irrelevant at this stage as it will be picked up subsequently in the convolutional part. 

Hence a lower dimensional representation makes sense.

#### b)

We need first of all a dense vocabulary for characters embeddings, so ![eqn](https://latex.codecogs.com/gif.latex?V_{\text{char}}&space;\times&space;e_{\text{char}});

then the overall weight matrix (plus the bias) of the 1D convolutional layer has a of total number of parameters equal to ![eqn](https://latex.codecogs.com/gif.latex?e_{\text{word}}&space;\times&space;e_{\text{char}}&space;\times&space;k&space;+&space;e_{\text{word}}).

The MaxPooling, ReLU and sigmoids layers have no parameter so only the other projection layers have ![eqn](https://latex.codecogs.com/gif.latex?e_{\text{word}}&space;\times&space;e_{\text{word}}&space;+&space;e_{\text{word}}) each, for both ![eqn](https://latex.codecogs.com/gif.latex?W_{\text{proj}}&space;,&space;W_{\text{gate}}).

In total thus we have ![eqn](https://latex.codecogs.com/gif.latex?V_{\text{char}}&space;\times&space;e_{\text{char}}&space;&plus;&space;e_{\text{word}}&space;\times&space;e_{\text{char}}&space;\times&space;k&space;&plus;&space;e_{\text{word}}&space;&plus;&space;2&space;\times&space;(&space;e_{\text{word}}\times&space;e_{\text{word}}&space;&plus;&space;e_{\text{word}}&space;))

For the word-level model instead we just have a dense embedding matrix of size ![eqn](https://latex.codecogs.com/gif.latex?V_{\text{word}}&space;\times&space;e_{\text{word}}).

This latter matrix though is way bigger, both because, as just discussed, the embeddings are bigger and because the vocabulary itself is much bigger! Think of how may words you can get by combining 'only' 96 characters!

This results in the embedding matrix for the word-level model in Figure 1 having 

![eqn](https://latex.codecogs.com/gif.latex?V_{\text{word}}&space;\times&space;e_{\text{word}}&space;=&space;50000&space;\times&space;256&space;=&space;12800000) parameters, 

while the character-level model of this assignment has

![eqn](https://latex.codecogs.com/gif.latex?V_{\text{char}}&space;\times&space;e_{\text{char}}&space;&plus;&space;e_{\text{word}}&space;\times&space;e_{\text{char}}&space;\times&space;k&space;&plus;&space;e_{\text{word}}&space;&plus;&space;2&space;\times&space;(&space;e_{\text{word}}&space;\times&space;e_{\text{word}}&space;&plus;&space;e_{\text{word}}&space;)&space;=&space;96&space;\times&space;50&space;&plus;&space;256&space;\times&space;50&space;\times&space;5&space;&plus;&space;50&space;&plus;&space;2&space;\times&space;(&space;50&space;\times&space;50&space;&plus;&space;50&space;=&space;73950))

which is almost 175 times less than the word-level model!

#### c)

CNN are known to better extract position invariants features and as such they're probably more suited to solve classification problems, like named entity recognition, sentiment analysis or genaral question like "does this sentence talk about food?", then RNNs; RNNs in turn, if properly set-up, can be better suited to keep track of context in a phrase which could be critical in language models applied to NMT or question-answering for example.

#### d)

In terms of pooling-through-time I think max pooling is better able to extract features from the convolutions results, ideally getting the information "has this feature/convolution been active in any part of the sequence" more sharply with respect to average pooling. If instead we were to use some sort of local pooling, then average pooling might make more sense to help us "smooth" the information contained in the convolution result.

### Implementation

See the files `vocab.py`, `utils.py`, `highway.py`, `cnn.py`, `model_embeddings.py` and `nmt_model.py`.

#### h,i) 

See `my_tests.py` for the tests I've written to ensure the new modules produce coherent/correct dimensions and that I can run a full pass across the new part of the network.

#### l)

As expected Perplexity on both train and dev sets reaches 1 and we get a final BLEU score of ~99.3, indicating severe overfit.

    epoch 100, iter 500, cum. loss 0.12, cum. ppl 1.01 cum. examples 200
    validation: iter 500, dev. ppl 1.001735
    Corpus BLEU: 99.29792465574434

## Part 2: Character-based LSTM decoder for NMT

### Implementation

#### a-f)

The implementation can be found mainly in `char_decoder.py`.

#### e)

As for Part 1, we observe

    epoch 200, iter 1000, cum. loss 0.53, cum. ppl 1.03 cum. examples 200
    validation: iter 1000, dev. ppl 1.006197
    Corpus BLEU: 99.29792465574434

#### f)

Training on a Colab Instance yield the following BLEU score:

    Corpus BLEU: 23.406770399035317

Took about 10 hours of training on a GPU runtime but it hit the early-stopping criterion at about epoch 25, so I suspect some more training (possibly with a lower lr) might improve on the score.
This pass the maximum marks available still, so I'll keep it at that.
(A Kaggle kernel is another option for training.)

## Part 3) Analyzing NMT Systems

### Written

#### a) 

    less vocab.json | grep tradu
        "traducir": 5152,
        "traduccin": 7014,
        "traduce": 8764,
        "traducido": 10498,
        "traductor": 15461,
        "traduccin.": 16679,
        "traducciones": 17686,
        "traductora": 22442,
        "traducen": 24809,
        "traducimos": 24810,
        "traduciendo": 25503,
        "traducida": 26380,
        "traduje": 30800,
        "traduzcan": 31349,
        "traductores": 31557,
        "traducidas": 35647,
        "traducirlo": 35673,
        "traduccin,": 40914,
        "traducir.": 43517,
        "traductora,": 48407,
        "traductora:": 48409,
        "traductor,": 48415,
        "traducirse": 48798,

So only *traducir* and *traduce* appears amongst those six variants, while *traduzco*, *traduces*, *traduzca* and *traduzcas* do not.

This is clearly bad because all these other forms will be tokenized as `<UNK>` when translating from Spanish to English!

Our hybrid model instead will form the word embeddings starting from the characters and thus will avoid the out-of-vocabulary problem completely. Moreover, since all these words have a common root, their embeddings will probably be close in some meaningful dimension in the embedding space.

#### b)

##### financial

| Word2Vec                       | CharCNN                            |
|--------------------------------|------------------------------------|
| economic                       | vertical                           |
| business                       | informal                           |
| markets                        | physical                           |
| banking                        | cultural                           |
| finance                        | electrical                         |


##### neuron

| Word2Vec                       | CharCNN                            |
|--------------------------------|------------------------------------|
| nerve                          | Newton                             |
| neural                         | George                             |
| cells                          | NBA                                |
| brain                          | Delhi                              |
| nervous                        | golden                             |


##### Francisco

| Word2Vec                       | CharCNN                            |
|--------------------------------|------------------------------------|
| san                            | France                             |
| jose                           | platform                           |
| diego                          | tissue                             |
| antonio                        | Foundation                         |
| california                     | microphone                         |


##### naturally

| Word2Vec                       | CharCNN                            |
|--------------------------------|------------------------------------|
| occurring                      | practically                        |
| readily                        | typically                          |
| humans                         | significantly                      |
| arise                          | mentally                           |
| easily                         | gradually                          |

##### expectation

| Word2Vec                       | CharCNN                            |
|--------------------------------|------------------------------------|
| norms                          | exception                          |
| assumptions                    | indication                         |
| policies                       | integration                        |
| inflation                      | separation                         |
| confidence                     | expected                           |

Often times it seems that Word2Vec capture more semantica meaning of the words, while the CharCNN find similarities between roots and endings of words (like financi**al**-vertic**al**, expecta**tion**-integra**tion**, natur**ally**-practic**ally** or **expect**ation-**expect**ed).
Additionally, Word2Vec finds words that usually go toghether, like *financial* and *markets*, *Francisco* and *san* or *naturally* and *occurring*.

This makes sense given that Word2Vec is trained to assign an embedding to a whole word based exclusively on the context around it, while our CharCNN embeds every single character and then, based on close-by characters, combines different character-embeddings into a word representation (completely disregarding surrounding words).

#### c)

`Reference Sp Sentence`: Hoy estoy aqu para hablarles sobre crculos y epifanas.
`Reference Eng Translation:` I'm here today to talk to you  about circles and epiphanies.

`A4_output:` I'm here to talk to you about circles and `<unk>`
`A5_output:` I'm here today to talk about circles and episodes.

This is an example of incorrect translation! The `<unk>` token has been replaced, but while the correct word is epiphanies, the CharCNN starts decoding correctly untill `epi` and then decides that, based on the corpus, epi**sodes** is much more likely than epi**phanies**. Beam search might have helped, but for the character-level decoder we only use greedy decoding for now.

----

`Reference Sp Sentence`: La razon por la que se someten a varias cirugas es que son una amenaza para nuestras categoras sociales.
`Reference Eng Translation:` The reason they're often subject to various kinds of surgeries  is because they threaten our social categories.

`A4_output:` The reason they `<unk>` these surgeries is that they are a threat to our social categories.
`A5_output:` The reason you get into various surgeries is that they're a threat for our social categories.

This time the translation for the word-level model fails to recognize the words *se someten* and thus produces an `<unk>` token. While not perfect (*get into surgeries* VS *subject to surgeries*), the translation for our model makes sense could be understood by an English speaker.

----

`Reference Sp Sentence`: Ohhhh.
`Reference Eng Translation:` Ohhhh.

`A4_output:` `<unk>`
`A5_output:` Oh.

In my opinion this is a perfect result. Not only the multiple *h*s in the input sentence confuse the word level model into predicting an `<unk>`, but the character-level model recognize the "semantic" *Oh* and translate it as such (because in English *Oh* is more frequent/probable than *Ohhhh*).
Again maybe beam search would have helped in producing more than one *h*, but I think this is a good result.
