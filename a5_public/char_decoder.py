#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
CS224N 2018-19: Homework 5
"""

import torch
import torch.nn as nn

class CharDecoder(nn.Module):
    def __init__(self, hidden_size, char_embedding_size=50, target_vocab=None):
        """ Init Character Decoder.

        @param hidden_size (int): Hidden size of the decoder LSTM
        @param char_embedding_size (int): dimensionality of character embeddings
        @param target_vocab (VocabEntry): vocabulary for the target language. See vocab.py for documentation.
        """
        ### YOUR CODE HERE for part 2a
        ###      - Initialize as an nn.Module.
        ###      - Initialize the following variables:
        ###        self.charDecoder: LSTM. Please use nn.LSTM() to construct this.
        ###        self.char_output_projection: Linear layer, called W_{dec} and b_{dec} in the PDF
        ###        self.decoderCharEmb: Embedding matrix of character embeddings
        ###        self.target_vocab: vocabulary for the target language
        ###
        ### Hint: - Use target_vocab.char2id to access the character vocabulary for the target language.
        ###       - Set the padding_idx argument of the embedding matrix.
        ###       - Create a new Embedding layer. Do not reuse embeddings created in Part 1 of this assignment.
        super(CharDecoder,self).__init__()
        
        self.hidden_size, self.char_embedding_size, self.target_vocab = hidden_size, char_embedding_size, target_vocab
        self.v_char_size = len(self.target_vocab.char2id)
        self.pad_token_idx = self.target_vocab.char2id['<pad>']


        self.charDecoder = nn.LSTM(
            input_size=self.char_embedding_size,
            hidden_size=self.hidden_size)

        self.char_output_projection = nn.Linear(
            in_features=self.hidden_size,
            out_features=self.v_char_size,
            bias=True)

        self.decoderCharEmb = nn.Embedding(self.v_char_size,self.char_embedding_size,padding_idx=self.pad_token_idx)

        self.forward_loss = torch.nn.CrossEntropyLoss(reduction='sum',ignore_index=self.pad_token_idx)

        ### END YOUR CODE


    
    def forward(self, input, dec_hidden=None):
        """ Forward pass of character decoder.

        @param input: tensor of integers, shape (length, batch)
        @param dec_hidden: internal state of the LSTM before reading the input characters. A tuple of two tensors of shape (1, batch, hidden_size)

        @returns scores: called s_t in the PDF, shape (length, batch, self.vocab_size)
        @returns dec_hidden: internal state of the LSTM after reading the input characters. A tuple of two tensors of shape (1, batch, hidden_size)
        """
        ### YOUR CODE HERE for part 2b

        scores = []
        X = self.decoderCharEmb(input)
        h_t, dec_hidden = self.charDecoder(X,dec_hidden) #note this is a nn.LSTM, not LSTMCell
        scores = self.char_output_projection(h_t)

        return scores, dec_hidden

        ### END YOUR CODE 


    def train_forward(self, char_sequence, dec_hidden=None):
        """ Forward computation during training.

        @param char_sequence: tensor of integers, shape (length, batch). Note that "length" here and in forward() need not be the same.
        @param dec_hidden: initial internal state of the LSTM, obtained from the output of the word-level decoder. A tuple of two tensors of shape (1, batch, hidden_size)

        @returns The cross-entropy loss, computed as the *sum* of cross-entropy losses of all the words in the batch.
        """
        ### YOUR CODE HERE for part 2c
        ### TODO - Implement training forward pass.
        ###
        ### Hint: - Make sure padding characters do not contribute to the cross-entropy loss.
        ###       - char_sequence corresponds to the sequence x_1 ... x_{n+1} from the handout (e.g., <START>,m,u,s,i,c,<END>).

        char_inputs = char_sequence[:-1]
        char_targets = char_sequence[1:] # teacher forcing
        scores, _ = self.forward(char_inputs,dec_hidden)
        return self.forward_loss(scores.transpose(1,2),char_targets) # we need to transpose to get correct dims for CEL

        ### END YOUR CODE

    def decode_greedy(self, initialStates, device, max_length=21):
        """ Greedy decoding
        @param initialStates: initial internal state of the LSTM, a tuple of two tensors of size (1, batch, hidden_size)
        @param device: torch.device (indicates whether the model is on CPU or GPU)
        @param max_length: maximum length of words to decode

        @returns decodedWords: a list (of length batch) of strings, each of which has length <= max_length.
                              The decoded strings should NOT contain the start-of-word and end-of-word characters.
        """

        ### YOUR CODE HERE for part 2d
        ### Implement greedy decoding.
        ### Hints:
        ###      - Use target_vocab.char2id and target_vocab.id2char to convert between integers and characters
        ###      - Use torch.tensor(..., device=device) to turn a list of character indices into a tensor.
        ###      - We use curly brackets as start-of-word and end-of-word characters. That is, use the character '{' for <START> and '}' for <END>.
        ###        Their indices are self.target_vocab.start_of_word and self.target_vocab.end_of_word, respectively.
        
        batch_size = initialStates[0].shape[1]
        start_token = self.target_vocab.start_of_word # self.target_vocab.char2id['{']
        end_token = self.target_vocab.end_of_word
        
        current_token = torch.tensor([start_token]*batch_size,device=device).unsqueeze(0) # Tensor: (1,batch_size) 
        current_states = initialStates
        
        output_word = torch.zeros(max_length,batch_size,device=device)

        for t in range(max_length):
            scores, current_states = self.forward(current_token,current_states)  # scores: (1,batch_size,vocab_size) 
            current_token = scores.argmax(dim=-1) # no need to do softmax actually, since it's a monotonic function
            output_word[t,:] = current_token

        decodedWords = ['']*batch_size
        for b in range(batch_size):
            word = output_word[:,b]
            # print(word); input('')
            for c in word:
                if c != end_token:
                    decodedWords[b] += self.target_vocab.id2char[c.item()]
                else:
                    break

        return decodedWords

        ### END YOUR CODE

