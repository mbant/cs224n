# Stanford CS224N: NLP with Deep Learning | Winter 2019

Course assignments done for self-education while following the [course](https://web.stanford.edu/class/cs224n/) through the [YT videolectures](https://www.youtube.com/playlist?list=PLoROMvodv4rOhcuXMZkNm7j3fVwBBY42z).

Assignments are grouped in separate folders.
