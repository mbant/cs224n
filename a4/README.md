# Assigment 4

[Questions](a4.pdf)

## Part 1 - Neural Machine Translation with RNNs

### a-h)

Coding done within the `utils.py`, `model_embeddings.py` and `nmt_model.py` files.

### i) 

Once the model is done training, by running `sh run.sh test` I get the following BLEU score on the test set.

    load test source sentences from [./en_es_data/test.es]
    load test target sentences from [./en_es_data/test.en]
    load model from model.bin
    Decoding: 100%|█████████████████████████████████████████████████████████████████████████| 8064/8064 [07:07<00:00, 18.86it/s]
    Corpus BLEU: 22.76133706303174

### j) 

Dot product attention is easy to conceptualize and fast to compute, but is restricted to vectors that have a similar (or the *same*)  meaning *i.e.* embeddings of different words or the hidden units of the RNN at different time-steps; otherwise computing 'plain' similarity between two vectors by means of orthogonalit makes little sense.

A generalization of this is the multiplicative attention, that allows for dot product attention if W is the identity, but in general can *learn* to adapt and better attend at the cost of the extra matrix multiplication; also it accomodates query and keys with different dimensions.

Additive attention is another type of attention where the *similarity* between query and keys is computed by **combining**  their effect rather than comparing them; The learnable parameter *v* (using the in-class notation) is then the one that specify, much like a gate in an LSTM cell, *where* attention needs to be focused the most, while the *W* matrices are used to pproject query and keys to a common representation. In theory this is close in computational complexity, but in practice can be much slower due to the ability of the GPU to parallelize matrix multiplication.

Accordin to [Vaswani et al., 2017](https://papers.nips.cc/paper/7181-attention-is-all-you-need.pdf) "Both variants perform similar for small dimensionality of the decoder states, but additive attention performs better for larger dimensions". The authors then hypothesize that this issue is due to "the dot products grow[ing] large in magnitude, pushing the softmax function into regions where it has extremely small gradients" and thus suggest a scaling factor to address the issue, which seems to work well in practice.

## Part 2 - Analyzing NMT Systems

### a) Identifying errors in NMT translations

#### i) 

**Source Sentence**: Aquı́ otro de mis favoritos, “La noche estrellada”.

**Reference Translation**: So another one of my favorites, “The Starry Night”.

**NMT Translation**: Here’s another favorite of my favorites, “The Starry Night”.

`another one` is an English construct, but in Spanish just one word `otro` suffices. Not sure why the model would produce a duplicate of `favorite` in this case.

I actually think that `Here’s` in this context is more correct than `So`, by the way..

#### ii) 

**Source Sentence**: Ustedes saben que lo que yo hago es escribir para los niños, y,
de hecho, probablemente soy el autor para niños, mas ledo en los EEUU.

**Reference Translation**: You know, what I do is write for children, and I’m probably America’s
most widely read children’s author, in fact.

**NMT Translation**: You know what I do is write for children, and in fact, I’m probably the
author for children, more reading in the U.S.

The model correctly picks that EEUU are the U.S., but a rearrangement of the words, as done here in the reference translation, is probably harder to do for a RNN which is producing one word at a time. Even for a seq2seq model apparently.

#### iii) 

**Source Sentence**: Un amigo me hizo eso – Richard Bolingbroke.

**Reference Translation**: A friend of mine did that – Richard Bolingbroke.

**NMT Translation**: A friend of mine did that – Richard `<unk>`

The problem in this case is simply the fixed vocabulary of the word-level language model. We don't have a token for 'Bolingbroke' and so the model produces `<unk>`. Character or sub-word level models aimto solve exactly this problem.

#### iv) 

**Source Sentence**: Solo tienes que dar vuelta a la manzana para verlo como una epifanı́a.

**Reference Translation**: You’ve just got to go around the block to see it as an epiphany.

**NMT Translation**: You just have to go back to the apple to see it as a epiphany.

`dar vuelta a la manzana` is a figure of speech that the model would have a hard time translating unless he's seen plenty of examples of it. The NMT instead recognize `manzana` and translate it literally as `apple`.

#### v) 

**Source Sentence**: Ella salvó mi vida al permitirme entrar al baño de la sala de profesores.

**Reference Translation**: She saved my life by letting me go to the bathroom in the teachers’ lounge.

**NMT Translation**: She saved my life by letting me go to the bathroom in the women’s room.

This is probably an error due to attention. When translating `al baño de la sala de ...` attention must have focused on `She` and thus the translation was `the women’s room`. Possibly due to the fact that `the teachers’ lounge` must not be so frequent.

Still the NMT fails completely to pick up `profesores` in the translation, signaling maybe that the seq2seq model actually gave little relevance to that work while encoding the sequence.

#### vi) 

**Source Sentence**: Eso es más de 100,000 hectáreas.

**Reference Translation**: That’s more than 250 thousand acres.

**NMT Translation**: That’s over 100,000 acres.

Converting between measurement units is probably outside the scope of an NMT model, and here the machine just picks up that we're talking about area measurements and translate `hectáreas` (which correspond to the very seldom used and probably not-in-vocabulary word `hectares`) to `acres` which is the most used word in this context.

### b)

More of the same ... not particularly useful to me so I'll probably come back to it at some point, but it's skipped for now..

### c) BLEU Score

Source Sentence **s**: el amor todo lo puede
Reference Translation **r<sub>1</sub>**: love can always find a way
Reference Translation **r<sub>2</sub>**: love makes anything possible

NMT Translation **c<sub>1</sub>**: the love can always do
NMT Translation **c<sub>2</sub>**: love can make anything possible

We'll use just bigrams with equal weights.

#### i)

##### **c<sub>1</sub>**:

unigrams = [ the, love, can, always, do ]
bigrams = [ (the love), (love can), (can always), (always do) ]

p<sub>1</sub> = ( 0 + 1 + 1 + 1 + 0 ) / 5 = 3/5
p<sub>2</sub> = ( 0 + 1 + 1 + 0 ) / 4 = 2/4

r<sup>*</sup> is r<sub>2</sub> in this case (both equally close, r<sub>2</sub> is shorter) with length 4

BP = 1

BLEU = 0.55

##### **c<sub>2</sub>**:

unigrams = [ love, can, make, anything, possible ]
bigrams = [ (love can), (can make), (make anything), (anything possible) ]

p<sub>1</sub> = ( 1 + 1 + 0 + 1 + 1 ) / 5 = 4/5
p<sub>2</sub> = ( 1 + 0 + 1 + 1 ) / 4 = 3/4

r<sup>*</sup> is r<sub>2</sub> in this case (both equally close, r<sub>2</sub> is shorter) with length 4

BP = 1

BLEU = 0.775

##### The better translation according to BLEU is c<sub>2</sub>, and I agree

#### ii)

The BLEU score for **c<sub>1</sub>** decrease slightly because of the BP, which decreases to 0.82, to a value of 0.45.
The score for **c<sub>2</sub>** though not only decreases for the same reason, but also its modified unigram and bigram precision decrease to 2/5 and 1/4 respectively, leading to a final BLEU = 0.27.

This makes **c<sub>1</sub>** the best translation according to BLEU, in contrast to my opinion.

#### iii)

Assuming that the quality of translation is good, there are usually many valid ways to translate the same sentence and words have synonyms that might be perfectly valid but penalized by BLEU in this case. With only one translation the brevity penalty becomes an issue as well.

#### iv)

Advantages:
* Quick to compute on a large corpus of sentences, to evaluate rapidly the capability of a NMT system. Human labour would instead take a lot of un-reausable time.
* As every standard it is a good a widely used measure, very useful to pit one NMT system against another to asses whether or not the community is making progresses.
* Overall it has a reported good correlation with human assessments of quality of translation, so it's not *that* bad.  

Disadvantages:
* There are typically many valid ways to translate the same text, so even a perfect translation may get penalized by BLEU.
* BLEU doesn't measure the grammar of a sentence at all, just presence/absence of words and n-grams.
* Because of the latter for example, it may give a small penalty for punctuation or other small changes, that instead completely change the meaning of a sentece!